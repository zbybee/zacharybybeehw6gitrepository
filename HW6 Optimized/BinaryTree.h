#pragma once
#include <memory>
//#include <string.h>
#include <string>
#include "KeyValue.h"
template <class K,class V>
class BinaryTree
{
public:
    ~BinaryTree();
    bool insert(KeyValue<K,V> value);
    bool remove(KeyValue<K,V> value,bool isKey);
    KeyValue<K,V> get(KeyValue<K,V> value,bool isKey);
    bool search(KeyValue<K,V> value,bool isKey);
    
    int numberNodes();
    int numberLeafNodes();
    int height();
    void display();
    void displayPreOrder();

private:
    class TreeNode
    {
    public :
        TreeNode(KeyValue<K,V> value) : data(value), left(nullptr), right(nullptr)
        {}
        KeyValue<K,V> data;

        std::shared_ptr<TreeNode> left;
        std::shared_ptr<TreeNode> right;
    };
    bool insert(std::shared_ptr<TreeNode>& node, std::shared_ptr<TreeNode> ptrNew);
    bool search(std::shared_ptr<TreeNode> node, KeyValue<K,V> value,bool isKey);
    KeyValue<K,V> get(std::shared_ptr<TreeNode> node, KeyValue<K,V> value,bool isKey);
    void display(std::shared_ptr<TreeNode> node);
    void displayPreOrder(std::shared_ptr<TreeNode> node);
    int numberNodes(std::shared_ptr<TreeNode>& node, int);
    int numberLeafNodes(std::shared_ptr<TreeNode>& node);
    int height(std::shared_ptr<TreeNode>& node);
    void remove(std::shared_ptr<TreeNode>& node, KeyValue<K,V> value, bool isKey);
    void remove(std::shared_ptr<TreeNode>& node);
    std::shared_ptr<TreeNode> m_root;//B TREE SHOULD BE TEMPLATE
};

template <class K,class V>
BinaryTree<K,V>::~BinaryTree()
{
   //NO NEED FOR DESTRUCTOR BECAUSE SHARED POINTERS ARE USED.     
}
template <class K,class V>
int BinaryTree<K,V>::numberLeafNodes()
{
    return numberLeafNodes(m_root);
}
template <class K,class V>
int BinaryTree<K,V>::numberLeafNodes(std::shared_ptr<TreeNode>& node)
{
    if (node == nullptr)
        return 0;
    if (node->right == nullptr && node->left == nullptr)
    {
        return 1;
    }
    else
    {
        return numberLeafNodes(node->right) + numberLeafNodes(node->left);
    }
}
template <class K,class V>
int BinaryTree<K,V>::numberNodes()
{
    return numberNodes(m_root,0);
}
template <class K,class V>
int BinaryTree<K,V>::numberNodes(std::shared_ptr<TreeNode>& node,int count)
{
    
    if (node->right != nullptr)
    {
        count = numberNodes(node->right, count);
    }
    if (node->left != nullptr)
    {
        count = numberNodes(node->left, count);
    }
    return count+1;
}
template <class K,class V>
int BinaryTree<K,V>::height()
{
    return height(m_root);
}
template <class K,class V>
int BinaryTree<K,V>::height(std::shared_ptr<TreeNode>& node)
{
    if (node == nullptr)
        return 0;
    int LeftHeight = height(node->left);
    int RightHeight = height(node->right);

    if (LeftHeight > RightHeight)
    {
        return LeftHeight + 1;
    }
    else
    {
        return RightHeight + 1;
    }
}

template <class K,class V>
bool BinaryTree<K,V>::insert(KeyValue<K,V> value)
{
    std::shared_ptr<TreeNode> ptrNew = std::make_shared<TreeNode>(value);
    return insert(m_root, ptrNew);
}
template <class K,class V>
bool BinaryTree<K,V>::remove(KeyValue<K,V> value, bool isKey)
{

    if (search(value, isKey))
    {
        remove(m_root, value,isKey);
        return true;
    }
    return false;

}
template <class K,class V>
bool BinaryTree<K,V>::search(KeyValue<K,V> value, bool isKey)
{
    return search(m_root, value, isKey);
}
template <class K,class V>
KeyValue<K,V> BinaryTree<K,V>::get(KeyValue<K,V> value, bool isKey)
{
    return get(m_root, value, isKey);
}
template <class K,class V>
bool BinaryTree<K,V>::insert(std::shared_ptr<TreeNode>& node, std::shared_ptr<TreeNode> ptrNew)
{
    if (node == nullptr)
    {
        node = ptrNew;
        return true;
    }
    else if (node->data.getKey() < ptrNew->data.getKey())
    {
        insert(node->right, ptrNew);
    }
    else if (node->data.getKey() > ptrNew->data.getKey())
    {
        insert(node->left, ptrNew);
    }
    if (ptrNew->data.getKey() == node->data.getKey())
        return false;
    else
    {
        return false;
    }
}
template <class K,class V>
bool BinaryTree<K,V>::search(std::shared_ptr<TreeNode> node, KeyValue<K,V> value, bool isKey)
{
    if (node == nullptr)
    {
        return false;
    }
    if(isKey)
    {
        if (node->data.getKey() == value.getKey())
        {
            return true;
        }
        if (node->data.getKey() < value.getKey())
        {
            return search(node->right, value,isKey);
        }
        else
        {
            return search(node->left, value,isKey);
        }
    }
    else
    {
        if (node->data.getValue() == value.getValue())
        {
           return true;
        }
        if (node->data.getValue() < value.getValue())
        {
            return search(node->right, value,isKey);
        }
        else
        {
            return search(node->left, value,isKey);
        }
    }


}

template <class K,class V>
KeyValue<K,V> BinaryTree<K,V>::get(std::shared_ptr<TreeNode> node, KeyValue<K,V> value, bool isKey)
{
    if (node == nullptr)
    {
        K k;
        V v;
        KeyValue<K,V> kv(k, v);
        return kv;
    }
    if(isKey)
    {
        if (node->data.getKey() == value.getKey())
        {
            return node->data;
        }
        if (node->data.getKey() < value.getKey())
        {
            return get(node->right, value,isKey);
        }
        else
        {
            return get(node->left, value,isKey);
        }
    }
    else
    {
        if (node->data.getValue() == value.getValue())
        {
            return node->data;
        }
        if (node->data.getValue() < value.getValue())
        {
            return get(node->right, value,isKey);
        }
        else
        {
            return get(node->left, value,isKey);
        }
    }


}
template <class K,class V>
void BinaryTree<K,V>::display()
{
    display(m_root);
}
template <class K,class V>
void BinaryTree<K,V>::displayPreOrder()
{
    displayPreOrder(m_root);
}

template <class K,class V>
void BinaryTree<K,V>::display(std::shared_ptr<TreeNode> node)
{
    if (node != nullptr)
    {
        display(node->left);
        std::cout << node->data.getKey() << std::endl;
        display(node->right);
    }
}

template <class K,class V>
void BinaryTree<K,V>::displayPreOrder(std::shared_ptr<TreeNode> node)
{
    if (node != nullptr)
    {
        std::cout << node->data.getKey() << std::endl;
        display(node->left);
        display(node->right);
    }
}
template <class K,class V>
void BinaryTree<K,V>::remove(std::shared_ptr<TreeNode>& node, KeyValue<K,V> value, bool isKey)
{
    if(isKey)
    {
        if (node->data.getKey() == value.getKey())
        {
            remove(node);
        }
        else if (node->data.getKey() < value.getKey())
        {
            remove(node->right, value,isKey);
        }
        else {
            remove(node->left, value, isKey);
        }
    }
        else
    {
        if (node->data.getValue() == value.getValue())
        {
            remove(node);
        }
        else if (node->data.getValue() < value.getValue())
        {
            remove(node->right, value,isKey);
        }
        else
        {
            remove(node->left, value,isKey);
        }
    }

}
template <class K,class V>
void BinaryTree<K,V>::remove(std::shared_ptr<TreeNode>& node)
{
    if (node->right == nullptr)
    {
        node = node->left;
    }
    else if (node->left == nullptr)
    {
        node = node->right;
    }
    else
    {
        std::shared_ptr<TreeNode> temp = node->right;

        while (temp->left)
        {
            temp = temp->left;
        }

        temp->left = node->left;
        node = node->right;
    }
}
