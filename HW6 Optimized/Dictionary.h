//
// Created by zbybee on 3/27/2017.
//

#ifndef GENERICDICTIONARY_DICTIONARY_H
#define GENERICDICTIONARY_DICTIONARY_H

#include <algorithm>
#include <exception>
#include <iostream>
#include <vector>
#include <functional>
#include "KeyValue.h"
#include "BinaryTree.h"
#include <typeinfo>

template <class K, class V>
class Dictionary {
public:
    Dictionary();
    Dictionary(int NumOfElements);
    Dictionary(const Dictionary& objDictionary);
    ~Dictionary();

    void add(K key,V value);
    int size();
    const KeyValue<K,V> getValueByKey(K key);
    const KeyValue<K,V> getKeyByValue(V value);

    const KeyValue<K,V> getByIndex(int index);
    const KeyValue<K,V> operator[](int index);//^^use the same method.
    void removeValueByKey(K);
    void removeKeyByValue(V);
private:
    BinaryTree<K,V> btOfKeyValues;
    std::vector<KeyValue<K,V>*> vctKeyValues;
    void AddKeyValueToArray(K,V);
    bool hasException;
    int allocatedSize;
    int numOfElementsInKeyValues;
    template <typename T>
    KeyValue<K,V> GenericFindValueInSearch(T key,bool isKey);

};


template <class K,class V>
Dictionary<K,V>::Dictionary()
{
    vctKeyValues = std::vector<KeyValue<K,V>*>(10);
    hasException=false;
    allocatedSize = 10;
    numOfElementsInKeyValues = 0;
}

template <class K,class V>
Dictionary<K,V>::Dictionary(int NumOfElements)
{
    vctKeyValues = std::vector<KeyValue<K,V>*>(NumOfElements);
    hasException=false;
    //keyValuesPtrArr = new KeyValue<K,V>*(NumOfElements);
    allocatedSize = NumOfElements;
    numOfElementsInKeyValues = 0;
}


template <class K,class V>
Dictionary<K,V>::Dictionary(const Dictionary& objDictionary)
{
    vctKeyValues = objDictionary.vctKeyValues;
    numOfElementsInKeyValues=objDictionary.numOfElementsInKeyValues;
    allocatedSize = objDictionary.allocatedSize;

}

template <class K,class V>
Dictionary<K,V>::~Dictionary()
{
    for(int i =0; i < vctKeyValues.size(); i++)
    {
        delete vctKeyValues.at(i);
    }
}

template <class K,class V>
void Dictionary<K,V>::AddKeyValueToArray(K key,V value)
{
    bool isInserted = btOfKeyValues.insert(KeyValue<K,V>(key,value));
    /*if(vctKeyValues.size() > numOfElementsInKeyValues)
    {
        vctKeyValues[numOfElementsInKeyValues] = new KeyValue<K,V>(key,value);
    }
    else {
        vctKeyValues.push_back(new KeyValue<K, V>(key, value));//This was the old way of doing it.
    }*/
    if(isInserted)
    numOfElementsInKeyValues++;
}


template <class K,class V>
void Dictionary<K,V>::add(K key,V value)
{
    try
    {
        KeyValue<K,V> kv = GenericFindValueInSearch<K>(key,true);
        hasException=true;
        throw std::overflow_error("Key alredy exists in vector. \nDictionary::Add");
    }
    catch (std::out_of_range ex)
    {
        AddKeyValueToArray(key,value);
    }
}

template <class K,class V>
int Dictionary<K,V>::size()
{
    return btOfKeyValues.numberNodes();
}

template <class K,class V>
const KeyValue<K,V> Dictionary<K,V>::getValueByKey(K key)
{
    return GenericFindValueInSearch<K>(key,true);
}

template <class K,class V>
const KeyValue<K,V> Dictionary<K,V>::getKeyByValue(V value)
{

    return GenericFindValueInSearch<V>(value,false);
}

template <class K,class V>
const KeyValue<K,V> Dictionary<K,V>::getByIndex(int index)
{
    if(numOfElementsInKeyValues > index && index >= 0)
    {
        return *vctKeyValues.at(index);
    }
    else
    {
        throw std::out_of_range("Index was out of range of Dictionary. \nDictionary::GetByIndex");
    }
}

template <class K,class V>
const KeyValue<K,V> Dictionary<K,V>::operator[](int index)
{
    return getByIndex(index);
}

template <class K,class V>
void Dictionary<K,V>::removeValueByKey(K key)
{
    V v;
    KeyValue<K,V> kv(key,v); //GenericFindValueInSearch<K>(key,true);
    bool isRemoved = btOfKeyValues.remove(kv,true);
    if(isRemoved)
    {
        numOfElementsInKeyValues--;
    }
    else
    {
        throw std::out_of_range("No Key was found in Vector. \nDictionary::GenericFindValueInSearch.");
    }
}

template <class K,class V>
void Dictionary<K,V>::removeKeyByValue(V value)
{
    K k;
    KeyValue<K,V> kv(k,value); //GenericFindValueInSearch<V>(value,false);
    bool isRemoved = btOfKeyValues.remove(kv,false);
    if(isRemoved)
    {
        numOfElementsInKeyValues--;
    }
    else
    {
        throw std::out_of_range("No Key was found in Vector. \nDictionary::GenericFindValueInSearch.");
    }
}

template <class K,class V>
template <typename T>
KeyValue<K,V> Dictionary<K,V>::GenericFindValueInSearch(T key, bool isKey) {
    K k;
    V v;
    if(isKey)
        k = key;
    else
        v = key;
    KeyValue<K,V> kv(k,v);
    if (btOfKeyValues.search(kv,isKey)) {
        return kv;
    } else {
        throw std::out_of_range("No Key was found in Vector. \nDictionary::GenericFindValueInSearch.");
    }
}
#endif //GENERICDICTIONARY_DICTIONARY_H
