//
// Created by zbybee on 4/3/2017.
//
#include "TestDictionary.h"

void TestingDictionary::Test()
{
    std::cout << "Testing Dictionary.\n";
    Dictionary<int,double>* dic = new Dictionary<int,double>();
    dic->add(2,5);
    dic->add(3,6);
    std::cout << "Testing GetKeyByValue.\n";
    try
    {
        KeyValue<int,double> x = dic->getKeyByValue(7);
        std::cout << "Error: keyValue was returned when key did not exist.\n";
    } catch (std::exception ex)
    {

    }

    std::cout << "Testing getByIndex.\n";
    try
    {
        KeyValue<int,double> x = dic->getByIndex(6);
        std::cout << "Error: keyValue was returned when key did not exist.\n";
    } catch (std::exception ex)
    {

    }

    std::cout << "Testing getValueByKey.\n";
    try
    {
        KeyValue<int,double> x = dic->getValueByKey(1);
        std::cout << "Error: KeyValue was returned when key did not exist.\n";
    } catch (std::exception ex)
    {

    }

    std::cout << "Testing removeValueByKey.\n";
    try
    {
        dic->removeValueByKey(6);
        std::cout << "Error: Keyitem was removed when keyValue Item did not exist.\n";
    } catch (std::exception ex)
    {

    }

    std::cout << "Testing removeKeyByValue.\n";
    try
    {
        dic->removeKeyByValue(2);
        std::cout << "Error: Keyitem was removed when keyValue Item did not exist.\n";
    } catch (std::exception ex)
    {

    }




}
