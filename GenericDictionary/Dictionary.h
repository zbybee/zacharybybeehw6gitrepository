//
// Created by zbybee on 3/27/2017.
//

#ifndef GENERICDICTIONARY_DICTIONARY_H
#define GENERICDICTIONARY_DICTIONARY_H

#include <algorithm>
#include <exception>
#include <iostream>
#include <vector>
#include <functional>
#include "KeyValue.h"

#include <typeinfo>

template <class K, class V>
class Dictionary {
public:
    Dictionary();
    Dictionary(int NumOfElements);
    Dictionary(const Dictionary& objDictionary);
    ~Dictionary();

    void add(K key,V value);
    int size();
    const KeyValue<K,V>& getValueByKey(K key);
    const KeyValue<K,V>& getKeyByValue(V value);

    const KeyValue<K,V>* getByIndex(int index);
    const KeyValue<K,V>* operator[](int index);//^^use the same method.
    void removeValueByKey(K);
    void removeKeyByValue(V);
    void removeByIndex(int index);
private:
    std::vector<KeyValue<K,V>*> vctKeyValues;
    void AddKeyValueToArray(K,V);
    template <typename T>
    bool isKeyOfProperType(T key,bool isKey);

    bool hasException;
    int allocatedSize;
    int numOfElementsInKeyValues;
    template <typename T>
    KeyValue<K,V>* GenericFindValueInSearch(T key,bool isKey);
};


template <class K,class V>
Dictionary<K,V>::Dictionary()
{
    vctKeyValues = std::vector<KeyValue<K,V>*>(10);
    hasException=false;
    allocatedSize = 10;
    numOfElementsInKeyValues = 0;
}

template <class K,class V>
Dictionary<K,V>::Dictionary(int NumOfElements)
{
    vctKeyValues = std::vector<KeyValue<K,V>*>(NumOfElements);
    hasException=false;
    //keyValuesPtrArr = new KeyValue<K,V>*(NumOfElements);
    allocatedSize = NumOfElements;
    numOfElementsInKeyValues = 0;
}


template <class K,class V>
Dictionary<K,V>::Dictionary(const Dictionary& objDictionary)
{
    vctKeyValues = objDictionary.vctKeyValues;
    numOfElementsInKeyValues=objDictionary.numOfElementsInKeyValues;
    allocatedSize = objDictionary.allocatedSize;

}

template <class K,class V>
Dictionary<K,V>::~Dictionary()
{
    for(int i =0; i < vctKeyValues.size(); i++)
    {
        delete vctKeyValues.at(i);
    }
}

template <class K,class V>
void Dictionary<K,V>::AddKeyValueToArray(K key,V value)
{
    if(vctKeyValues.size() > numOfElementsInKeyValues)
    {
        vctKeyValues[numOfElementsInKeyValues] = new KeyValue<K,V>(key,value);
    }
    else {
        vctKeyValues.push_back(new KeyValue<K, V>(key, value));
    }
    numOfElementsInKeyValues++;
}


template <class K,class V>
void Dictionary<K,V>::add(K key,V value)
{
    try
    {
        KeyValue<K,V>* kv = GenericFindValueInSearch<K>(key,true);
        hasException=true;
        throw std::overflow_error("Key alredy exists in vector. \nDictionary::Add");
    }
    catch (std::out_of_range ex)
    {
        AddKeyValueToArray(key,value);
    }
}

template <class K,class V>
int Dictionary<K,V>::size()
{
    return vctKeyValues.size();
}

template <class K,class V>
const KeyValue<K,V>& Dictionary<K,V>::getValueByKey(K key)
{
    return *GenericFindValueInSearch<K>(key,true);
}

template <class K,class V>
const KeyValue<K,V>& Dictionary<K,V>::getKeyByValue(V value)
{

    return *GenericFindValueInSearch<V>(value,false);
}

template <class K,class V>
const KeyValue<K,V>* Dictionary<K,V>::getByIndex(int index)
{
    if(numOfElementsInKeyValues > index && index >= 0)
    {
        return vctKeyValues.at(index);
    }
    else
    {
        throw std::out_of_range("Index was out of range of Dictionary. \nDictionary::GetByIndex");
    }
}

template <class K,class V>
const KeyValue<K,V>* Dictionary<K,V>::operator[](int index)
{
    return getByIndex(index);
}

template <class K,class V>
void Dictionary<K,V>::removeValueByKey(K key)
{
    KeyValue<K,V>* kv = GenericFindValueInSearch<K>(key,true);
    vctKeyValues.erase(std::remove(vctKeyValues.begin(),vctKeyValues.end(),kv),vctKeyValues.end());
    numOfElementsInKeyValues--;
}

template <class K,class V>
void Dictionary<K,V>::removeKeyByValue(V value)
{
    KeyValue<K,V>* kv = GenericFindValueInSearch<V>(value,false);
    vctKeyValues.erase(std::remove(vctKeyValues.begin(),vctKeyValues.end(),kv),vctKeyValues.end());
    numOfElementsInKeyValues--;
}
template <class K,class V>
void Dictionary<K,V>::removeByIndex(int index)
{
    if(vctKeyValues.size() > index && index >= 0)
    {
        vctKeyValues.erase(std::remove(vctKeyValues.begin(),vctKeyValues.end(),vctKeyValues.at(index)),vctKeyValues.end());
        numOfElementsInKeyValues--;
    }
    else
    {
        throw std::out_of_range("Index was out of range of Dictionary. \nDictionary::RemoveByIndex");
    }


    //TODO: ERROR CHECKING FOR NON VALUES OR VALUES NOT IN VECTOR

}

template <class K,class V>
template <typename T>
bool Dictionary<K,V>::isKeyOfProperType(T key,bool isKey)
{
    bool isValid = false;
    if(isKey)
    {
        if(std::is_same<T, K>::value)
        {
            isValid = true;
        }
    }
    else
    {
        if(std::is_same<T, V>::value)
        {
            isValid = true;
        }
    }
    return isValid;
}

template <class K,class V>
template <typename T>
KeyValue<K,V>* Dictionary<K,V>::GenericFindValueInSearch(T key, bool isKey)
{
    bool isValid = isKeyOfProperType<T>(key, isKey);

    bool hasFound = false;
    for(int i = 0; i < numOfElementsInKeyValues; i++)
    {
        if(vctKeyValues.at(i)->isEqual(key,isKey))
        {
            return vctKeyValues.at(i);
        }
    }
    hasException=true;
    throw std::out_of_range("No Key was found in Vector. \nDictionary::GenericFindValueInSearch.");
}

#endif //GENERICDICTIONARY_DICTIONARY_H
