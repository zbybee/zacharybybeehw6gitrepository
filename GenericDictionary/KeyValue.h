//
// Created by zbybee on 3/27/2017.
//

#ifndef GENERICDICTIONARY_KEYVALUE_H
#define GENERICDICTIONARY_KEYVALUE_H
#include <algorithm>
#include <exception>
#include <iostream>
#include <typeinfo>
#include <iterator>


template <class K,class V>
class KeyValue {
public:
    KeyValue(K key,V value);
    KeyValue(const KeyValue& objKeyValue);
    ~KeyValue();

    template <typename T>
    bool isEqual(T type,bool isKey);


    const K getKey() {return key;}
    const V getValue() {return Value;}


private:
    K key;
    V Value;


};

template <class K,class V>
KeyValue<K,V>::KeyValue(K _key,V _value)
{
    key = _key;
    Value = _value;
}

template <class K,class V>
KeyValue<K,V>::KeyValue(const KeyValue& objKeyValue)
{
    this->key = objKeyValue.key;
    this->Value = objKeyValue.Value;
}
template <class K,class V>
KeyValue<K,V>::~KeyValue()
{

}

template <class K,class V>
template <typename T>
bool KeyValue<K,V>::isEqual(T type, bool isKey)//public
{
    if (std::is_same<T, K>::value && isKey) {
        return this->key == type;
    } else if (std::is_same<T, V>::value && !isKey) {

        return this->Value == type;
    } else {
        throw std::invalid_argument("Type provided was not part of KeyValue Set. \nKeyValue::IsEqual<T>.");
    }
}


#endif //GENERICDICTIONARY_KEYVALUE_H
